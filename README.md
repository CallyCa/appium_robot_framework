# Appium Testing with Robot Framework

Execute os seguintes comando abaixo:

```bash
$ pip install -r requeriments.txt
```

## Instalação do Appium

```
$ npm install -g appium appium-doctor
```

## Executar Appium-Server

```
$ python resources/server.py
```

## Executar os testes:
```bash

$ robot features/android/*.test.robot
ou
$ robot features/ios/*.test.robot
```

## Relatórios/Reports
Ao executar os testes, os mesmos ao finalizarem irão estar gerando os reports dos testes na raiz do projeto. 

Para isto, antes de abrir os reports execute o seguinte comando abaixo para estar movendo estes reports para o diretório `/reports`:

```bash
$ python resources/tasks.py
```