import os

class ServerPy():

    def appium_server():
        try:
            os.system("appium")
        except KeyboardInterrupt:
            print("Server stopped.")


if __name__ == '__main__':
    try:
        ServerPy.appium_server()
    except (TypeError, ValueError):
        print(__doc__)
