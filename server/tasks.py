from pathlib import Path
import shutil


def move_docs():

    log = Path('./log.html')
    output = Path('./output.xml')
    report = Path('./report.html')
    dest = Path('.') / 'reports'
    
    move_log = shutil.move(str(log), str(dest))
    move_output = shutil.move(str(output), str(dest))
    move_report = shutil.move(str(report), str(dest))
    
    print(move_log, move_output, move_report)

move_docs()